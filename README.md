# Sección 4: Navegación dentro de la App

Dentro de esta seccion del curso aprendi a manejar la navegación sencilla entre diferentes rutas de la aplicación, las cuales estan definidas en el archivo `App.js`.
Se nos explica que el componente `navigator`, al ser creado con la función _createStackNavigator_ propia de `react-navigation`, y al definir nuestras rutas nosotros podemos pasarle sus
propiedades a los componentes de las rutas especificadas, como en el caso de HomeScreen:

```jsx
const HomeScreen = ({navigation}) => {
  return(
    <View>
      <Text style={styles.text}>Botones con Navegacion :D</Text>
      <Button
        title = "Vamos a los componentes :D" 
        onPress = {()=> navigation.navigate('Componentes')}
      />
      <Button
        title = "Vamos a la Listita" 
        onPress = {()=> navigation.navigate('ListScreen')}
      />     
    </View>
  );
};

```



Hemos modificado la estructura de nuestro componente para que muestre dos botones primitivos y un texto. Los botones primitivos son componentes que ejecutan un evento al hacer click en ellos, sin embargo no son muy estilizables.
- La propiedad `title` nos permite asignare texto al botón.
- La propiedad `onPress` es una función sobrecargada y en este caso se usa para navegar entre componentes enrutados.

`navigation` es una propiedad de las diversas propiedades de nuestro StackNavigator definido en `App.js`. este objeto tiene el método _navigate()_ el cual nos genera ese cambio entre pantallas.

**Notas**
- Gracias a las llaves `{}` es que podemos obtener solamente la propiedad `navigation` de todas las propiedades de nuestro `navigator`
- En esta sección se nos enseña otro componente, el `TouchableOpacity`, que tambien es un componente primitivo, este puede agrupar varios componentes, es mas estilizable que el botón y tambien tiene métodos para poder hacer click en el.  
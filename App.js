import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import Components from './src/screens/componentScreen';
import ListitaFlat from './src/screens/ListScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Componentes: Components,
    ListScreen: ListitaFlat
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'Seccion 4'
    }
  }
);

export default createAppContainer(navigator);

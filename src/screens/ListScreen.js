import React from 'react';
import {StyleSheet, Text, FlatList} from 'react-native';

const ListitaFlat = () =>
{
    const amixes = [
        { friend: "Amix #1", age: 40},
        { friend: "Amix #2", age: 40},
        { friend: "Amix #3", age: 40},
        { friend: "Amix #4", age: 40},
        { friend: "Amix #5", age: 40},
        { friend: "Amix #6", age: 40},
        { friend: "Amix #7", age: 40},
        { friend: "Amix #8", age: 40},
    ];
    return (
        <FlatList keyExtractor = {amix => amix.friend}
            data = {amixes} 
            renderItem = {({item}) => {
                return <Text style={estilos.textStyle}>Amigo: {item.friend} - Edad: {item.age}</Text>;
            }}
        />
    );
};

const estilos = StyleSheet.create(
{
    textStyle:
    {
        marginVertical: 10
    }
}
);

export default ListitaFlat;